package task3.services;

import task3.daos.NodeDAO;
import task3.entity.Node;

import java.util.List;

public class NodeService {
    private NodeDAO nodesDao = new NodeDAO();

    public NodeService() {
    }

    public Node findNode(int id) {
        return nodesDao.findById(id);
    }

    public void saveNode(Node node) {
        nodesDao.save(node);
    }

    public void deleteNode(Node node) {
        nodesDao.delete(node);
    }

    public void updateNode(Node node) {
        nodesDao.update(node);
    }

    public List<Node> findAllNodes() {
        return nodesDao.findAll();
    }

    public List<Node> findNearestNodes(int lat,int lon,int radius,int limit){return nodesDao.findNearest(lat, lon, radius,limit);}
}
