package task3.contollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import task3.entity.Node;
import task3.services.NodeService;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NearestController {
    private EntityManager entityManager;

    @Autowired
    public NearestController(EntityManager manager) {
        this.entityManager = manager;
    }

    @GetMapping("/near")
    public List<Node> getNear(
            @RequestParam(name = "radius") double radius,
            @RequestParam(name = "latitude") int latitude,
            @RequestParam(name = "longitude") int longitude,
            @RequestParam(name = "limit") int limit) {
        NodeService ns = new NodeService();
        System.out.println("LOL");
        List<Node> lnodes = ns.findNearestNodes(10,10,1000,100);
        for (Node nod : lnodes) {
            System.out.println(nod);
            System.out.println("LOL");
        }
        System.out.println("LOL");
        return lnodes;
    }


    @GetMapping("/nearest")
    public ResponseEntity getNearest(
            @RequestParam(name = "radius") int radius,
            @RequestParam(name = "latitude") int latitude,
            @RequestParam(name = "longitude") int longitude,
            @RequestParam(name = "limit") int limit) {

        String queryCode = String.format("SELECT * " +
                "FROM Nodes " +
                "WHERE earth_distance(ll_to_earth(%d, %d), ll_to_earth(lat, lon)) < %d " +
                "LIMIT %d", latitude, longitude, radius, limit);


        Query query = entityManager.createNativeQuery(queryCode, Node.class);
        List<Node> nodes = query.getResultList();


        return ResponseEntity
                .status(HttpStatus.OK)
                .body(nodes);
    }
}
