package task3.contollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import task3.entity.Node;
import task3.reps.NodeRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NodeController {

    @Autowired
    NodeRepository nodeRepository;

    @GetMapping("/nodes")
    public List<Node> getAllNodes() {
        return nodeRepository.findAll();
    }


    @PostMapping("/nodes")
    public Node createNote(@Valid @RequestBody Node node) {
        return nodeRepository.save(node);
    }

    @GetMapping("/nodes/{id}")
    public Node getNoteById(@PathVariable(value = "id") Long nodeId) throws Exception {
        return nodeRepository.findById(nodeId)
                .orElseThrow(Exception::new);
    }

    @PutMapping("/nodes/{id}")
    public Node updateNote(@PathVariable(value = "id") Long nodeId,
                           @Valid @RequestBody Node nodeDetails) throws Exception {

        Node node = nodeRepository.findById(nodeId)
                .orElseThrow(Exception::new);

        node.setUser(nodeDetails.getUser());

        System.out.println(nodeDetails);

        Node updatedNote = nodeRepository.save(node);
        return updatedNote;
    }

    @DeleteMapping("/nodes/{id}")
    public ResponseEntity<?> deleteNode(@PathVariable(value = "id") Long nodeId) throws Exception {
        Node note = nodeRepository.findById(nodeId)
                .orElseThrow(Exception::new);

        nodeRepository.delete(note);

        return ResponseEntity.ok().build();
    }
    // Get All Notes

    // Create a new Note

    // Get a Single Note

    // Update a Note

    // Delete a Note
}