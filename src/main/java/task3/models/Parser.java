package task3.models;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import task3.App;
import task3.adapters.MapAdapter;
import task3.entity.Node;

public class Parser {

    private static final Logger log = LoggerFactory.getLogger(App.class);
    private XMLStreamReader reader;
    XMLStreamReader xsr;

    public Parser(){};

    public void initXSR(){
        XMLInputFactory xif = XMLInputFactory.newFactory();
        try {
            xsr = xif.createXMLStreamReader(new FileInputStream("/Users/murzeg/dev/task3/src/main/resources/RU-NVS.osm"));
        } catch (XMLStreamException e) {
            log.error("xml reader trouble");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            log.error("wrong path");
            e.printStackTrace();
        }
        xsr = new XsiTypeReader(xsr);
    }

    public List<Node> getNodes(int size) throws Exception {
        List<Node> nodes = new ArrayList<Node>();
        if(size <= 0) size = 10;
        for(int i = 0; i < size; i++) {
            nodes.add(getNode());
        }
        return nodes;
    }

    public Node getNode() throws Exception {
        xsr.nextTag();
        while (!xsr.getLocalName().equals("node")) {
            xsr.nextTag();
        }

        JAXBContext jc = JAXBContext.newInstance(Node.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        unmarshaller.setAdapter(new MapAdapter());
        Node node = (Node) unmarshaller.unmarshal(xsr);
//        System.out.println(node);

        return node;
    }

    private class XsiTypeReader extends StreamReaderDelegate {

        public final java.lang.String SCHEMA_INSTANCE_URL = "http://www.w3.org/2001/XMLSchema-instance";

        public XsiTypeReader(XMLStreamReader reader) {
            super(reader);
        }

        @Override
        public String getAttributeNamespace(int arg0) {
            if ("type".equals(getAttributeLocalName(arg0))) {
                return SCHEMA_INSTANCE_URL;
            }
            return super.getAttributeNamespace(arg0);
        }
    }

}