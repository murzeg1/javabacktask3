package task3.reps;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import task3.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);
}
