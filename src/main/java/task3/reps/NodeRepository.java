package task3.reps;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import task3.entity.Node;

@Repository
public interface NodeRepository extends JpaRepository<Node, Long> {

}
