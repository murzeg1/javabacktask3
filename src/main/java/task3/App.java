package task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import task3.entity.Node;
import task3.models.Parser;
import task3.services.NodeService;

import java.util.List;

@SpringBootApplication
@EnableJpaAuditing
public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

    public void m() throws Exception {
        App xmlReader = new App();
        Parser pars = new Parser();
        pars.initXSR();
        log.info("Ready to get Node");
        Node node = pars.getNode();
        System.out.println(node);

        List<Node> ns = pars.getNodes(100);
//        for (Node nod : ns) System.out.println(nod);

        node = new Node(1,1,"da",1,"murzeg",1,1,1);

        NodeService nodeService = new NodeService();
//        for (Node n:
//             ns) {
//            nodeService.saveNode(n);
//        }
        System.out.println("LOL");
      List<Node> lnodes = nodeService.findNearestNodes(10,10,1000,100);
      for (Node nod : lnodes) {
          System.out.println(nod);
          System.out.println("LOL");
      }
        System.out.println("LOL");
        return;
    }

//    public Node getNode() throws Exception {
//        XMLInputFactory xif = XMLInputFactory.newFactory();
//        XMLStreamReader xsr = xif.createXMLStreamReader(new FileInputStream("/Users/murzeg/dev/task3/src/main/resources/RU-NVS.osm"));
//        xsr = new XsiTypeReader(xsr);
//
//        xsr.nextTag();
//        while (!xsr.getLocalName().equals("node")) {
//            xsr.nextTag();
//        }
//
//
//        JAXBContext jc = JAXBContext.newInstance(Node.class);
//        Unmarshaller unmarshaller = jc.createUnmarshaller();
//        unmarshaller.setAdapter(new MapAdapter());
//        Node node = (Node) unmarshaller.unmarshal(xsr);
//
//        System.out.println(node);
//
//
//        if(xsr.getLocalName().equals("tag")){
//
//            jc = JAXBContext.newInstance(Tag.class);
//            unmarshaller = jc.createUnmarshaller();
//            unmarshaller.setAdapter(new MapAdapter());
//            Tag tag = (Tag) unmarshaller.unmarshal(xsr);
//
//            System.out.println(tag);
//        }
//
//        return node;
//    }

//    public void findTags() throws  Exception{
//        XMLInputFactory xif = XMLInputFactory.newFactory();
//        XMLStreamReader xsr = xif.createXMLStreamReader(new FileInputStream("/Users/murzeg/dev/task3/src/main/resources/RU-NVS.osm"));
//        xsr = new XsiTypeReader(xsr);
//
//        for(int i = 0; i < 1000; i++){
//            xsr.nextTag();
//            if(xsr.getLocalName().equals("tag")){
//                System.out.println("FOUND");
//            }
//            else{
//                System.out.println("Not Found");
//            }
//        }
//    }
//
//    public void printNodes() throws Exception{
//        XMLInputFactory xif = XMLInputFactory.newFactory();
//        XMLStreamReader xsr = xif.createXMLStreamReader(new FileInputStream("/Users/murzeg/dev/task3/src/main/resources/RU-NVS.osm"));
//        xsr = new XsiTypeReader(xsr);
//
//        Node node = new Node();
//        for(int i = 0; i < 10000; i ++) {
//            xsr.nextTag();
//            while (!xsr.getLocalName().equals("node")) {
//                xsr.nextTag();
//            }
//
//            JAXBContext jc = JAXBContext.newInstance(Node.class);
//            Unmarshaller unmarshaller = jc.createUnmarshaller();
//            node = (Node) unmarshaller.unmarshal(xsr);
//
////            System.out.println(node);
//            if (!node.getTags().isEmpty()) {
//                System.out.println("THIS");
//                System.out.println(node);
//            }
//        }
//    }
//
//    private class XsiTypeReader extends StreamReaderDelegate {
//
//        public final java.lang.String SCHEMA_INSTANCE_URL = "http://www.w3.org/2001/XMLSchema-instance";
//
//        public XsiTypeReader(XMLStreamReader reader) {
//            super(reader);
//        }
//
//        @Override
//        public String getAttributeNamespace(int arg0) {
//            if ("type".equals(getAttributeLocalName(arg0))) {
//                return SCHEMA_INSTANCE_URL;
//            }
//            return super.getAttributeNamespace(arg0);
//        }
//    }
}


