package task3.entity;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.HashMap;
import java.util.Map;


@Entity
@Table(name = "nodes")
@XmlType(propOrder = {"id", "version", "timestamp", "uid", "user", "changeset", "lat", "lon"})
@EntityListeners(AuditingEntityListener.class)
@XmlRootElement(name = "node")
public class Node {

    @Id
    private long id;
    @Column(name = "version")
    private int version;
    @Column(name = "timestamp")
    private String timestamp;
    @Column(name = "uid")
    private long uid;
    @Column(name = "userrr")
    private String user;
    @Column(name = "changeset")
    private long changeset;
    @Column(name = "lat")
    private double lat;
    @Column(name = "lon")
    private double lon;


//    @XmlElement(name = "tag")
//    @XmlJavaTypeAdapter(MapAdapter.class)
//    public Map<String, String> getTags() {
//        return tags;
//    }
//
//
//    public void setTag(HashMap<String, String> tags) {
//        this.tags = tags;
//    }
//
//    private Map<String, String> tags = new HashMap<String, String>();



    public long getId() {
        return id;
    }

    @XmlAttribute
    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }


    @XmlAttribute
    public void setVersion(int version) {
        this.version = version;
    }


    public String getTimestamp() {
        return timestamp;
    }


    @XmlAttribute
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    public long getUid() {
        return uid;
    }


    @XmlAttribute
    public void setUid(long uid) {
        this.uid = uid;
    }


    public String getUser() {
        return user;
    }


    @XmlAttribute
    public void setUser(String user) {
        this.user = user;
    }


    public long getChangeset() {
        return changeset;
    }

    @XmlAttribute
    public void setChangeset(long changeset) {
        this.changeset = changeset;
    }


    public double getLat() {
        return lat;
    }

    @XmlAttribute
    public void setLat(double lat) {
        this.lat = lat;
    }


    public double getLon() {
        return lon;
    }

    @XmlAttribute
    public void setLon(double lon) {
        this.lon = lon;
    }

    public Node(){}

    public Node(long id, int version, String timestamp, long uid, String user, long changeset,
                double lat, double lon)
    {
        this.id = id;
        this.version = version;
        this.timestamp = timestamp;
        this.uid = uid;
        this.user = user;
        this.changeset = changeset;
        this.lat = lat;
        this.lon = lon;
//        this.tags = tags;
    }

    @Override
    public String toString()
    {
        return "NodeINFO: id: " + id + " version: " + version + " timestamp: " + uid + " user: " + user + " changeset: "
                + changeset + " lat: " + lat + " lon: " + lon;
    }
}
