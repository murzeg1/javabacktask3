package task3.entity;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"k", "v"})
@XmlRootElement(name = "tag")
public class Tag {

    public String getK() {
        return k;
    }

    @XmlAttribute
    public void setK(String k) {
        this.k = k;
    }

    public String getV() {
        return v;
    }

    @XmlAttribute
    public void setV(String v) {
        this.v = v;
    }

    private String k;
    private String v;

    public Tag(){};

    public Tag(String k, String v){
        this.k = k;
        this.v = v;
    }

    @Override
    public String toString(){
        return "K = " + k + "V = " + v;
    }
}
