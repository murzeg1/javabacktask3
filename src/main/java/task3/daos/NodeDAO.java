package task3.daos;

import org.hibernate.Session;
import org.hibernate.Transaction;
import task3.entity.Node;
import task3.utils.HibernateSessionFactoryUtil;

import java.util.List;

public class NodeDAO {
    public Node findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Node.class, id);
    }

    public void save(Node node) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(node);
        tx1.commit();
        session.close();
    }

    public void update(Node node) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(node);
        tx1.commit();
        session.close();
    }

    public void delete(Node node) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(node);
        tx1.commit();
        session.close();
    }


    public List<Node> findAll() {
//        List<Node> nodes = (List<Node>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From nodes").list();
        List<Node> nodes = HibernateSessionFactoryUtil.getSessionFactory().openSession().createCriteria(Node.class).list();
        return nodes;
    }

    public List<Node> findNearest(int latitude, int longitude, int radius, int limit ){
        String queryString = String.format(
                "FROM nodes " +
                "WHERE earth_distance(ll_to_earth(%d, %d), ll_to_earth(lat, lon)) < %d ", latitude, longitude, radius);
        List<Node> nodes = (List<Node>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery(queryString).list();
        return nodes;
    }
}